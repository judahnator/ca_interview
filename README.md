# File Processor Exercise

## Setup:

### Repository
First things first, clone this repository somewhere.

```bash
git clone git@gitlab.com:judahnator/ca_interview.git;
cd ca_interview;
```

### Database
Next you will need to set up a database. A simple sqlite database will work fine for our needs.

`touch var/data.db`

Configure this database by editing the `.env` file and ensuring all `DATABASE_URL` lines are commented out, then ensure the following line exists and is not commented out:

```
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
```

Finally, you need to run the migrations to get the table set up properly. To do this just run:

```bash
php bin/console doctrine:migrations:migrate --no-interaction
```

### Currency Converter
In order to interact with the currency converter API we will need an API key. Obtain one from [the free currency converter website](https://free.currencyconverterapi.com/).

Once you have your API key, lets drop it into a "local env" file like so:

```bash
echo "CURRENCY_CONVERTER_API_KEY=your api key here" > .env.local
```

### Composer
Run `composer install`.

### Internal Server
When you finish the above, start up a local instance of a development server with the following:
```bash
php -S localhost:8080 -t public/
```

## Usage

Open your web browser and visit `http://localhost:8080` and you should be greeted with the landing page. Its pretty bare bones, you get the gist of it.

Browse for your CSV file and click on "Submit". If all goes well you should see the table populate with the data submitted. Every time you re-submit new data the internal "product" table is truncated and the new data written.

## Potential problems

If you are missing any extensions then composer should tell you during installation. However, there is a dependency on the "sqlite" extension that I did not explicitly tell composer to require, because you could opt to use a different database instead.

If you run into issues, you could check to see if you have the extension installed by running `dpkg -l | grep sqlite` - your mileage may vary if using non-debian-based systems. 
