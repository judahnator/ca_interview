<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function avgCost(): float
    {
        $result = $this->getEntityManager()
            ->createQuery('SELECT AVG(p.cost) AS average_cost FROM App\Entity\Product p')
            ->getSingleScalarResult();
        return is_null($result) ? 0.0 : (float)$result;
    }

    public function avgPrice(): float
    {
        $result = $this->getEntityManager()
            ->createQuery('SELECT AVG(p.price) AS average_price FROM App\Entity\Product p')
            ->getSingleScalarResult();
        return is_null($result) ? 0.0 : (float)$result;
    }

    public function totalQty(): int
    {
        $result = $this->getEntityManager()
            ->createQuery('SELECT SUM(p.qty) AS total_qty FROM App\Entity\Product p')
            ->getSingleScalarResult();
        return is_null($result) ? 0 : (int)$result;
    }

    public function avgProfitMargin(): float
    {
        $result = $this->getEntityManager()
            ->createQuery('SELECT AVG(p.price - p.cost) AS average_profit_margin FROM App\Entity\Product p')
            ->getSingleScalarResult();
        return is_null($result) ? 0.0 : (float)$result;
    }

    public function totalProfit(): float
    {
        $result = $this->getEntityManager()
            ->createQuery('SELECT SUM((p.price - p.cost) * p.qty) AS average_profit_margin FROM App\Entity\Product p')
            ->getSingleScalarResult();
        return is_null($result) ? 0.0 : (float)$result;
    }

    public function truncate(): void
    {
        $this->getEntityManager()->createQuery('DELETE FROM App\Entity\Product')->execute();
    }
}
