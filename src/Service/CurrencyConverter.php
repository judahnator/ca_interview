<?php declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class CurrencyConverter
{
    public function __construct(
        private CacheInterface $cache,
        private HttpClientInterface $client,
        private string $apiKey,
    ) {}

    public function getRatio(string $from, string $to): float
    {
        return (float)$this->cache->get("currency_{$from}_{$to}", function (ItemInterface $item) use ($from, $to): float {
            $item->expiresAfter(new \DateInterval('PT5M'));
            return $this->client
                ->request('GET', "https://free.currconv.com/api/v7/convert?q={$from}_{$to}&compact=ultra&apiKey={$this->apiKey}")
                ->toArray()["{$from}_{$to}"];
        });
    }
}
