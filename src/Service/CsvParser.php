<?php declare(strict_types=1);

namespace App\Service;

final class CsvParser
{
    public function __construct(private \SplFileInfo $file)
    {
        if (!$this->file->isReadable()) {
            throw new \RuntimeException('Cannot read from the provided file.');
        }
    }

    public function parse(string ...$headers): \IterableCollection
    {
        $headers = array_flip($headers);

        $records = $this->readFile();

        /** @var ?array $fileHeaders */
        $fileHeaders = $records->current();

        return \IterableCollection::wrap($records)

            // map file headers onto each row
            ->map(static function (array $row) use ($fileHeaders): array {
                $new = [];
                foreach ($fileHeaders as $index => $header) {
                    $new[$header] = $row[$index];
                }
                return $new;
            })

            // filter undesired columns
            ->map(static function (array $row) use ($headers): array {
                return array_intersect_key($row, $headers);
            })

            // pop the header row off
            ->filter(static fn ($_, $key): bool => $key !== 0);
    }

    private function readFile(): \Generator
    {
        $handle = fopen($this->file->getRealPath(), 'r');
        while (($record = fgetcsv($handle)) !== false) {
            yield $record;
        }
    }
}