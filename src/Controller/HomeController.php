<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\CsvParser;
use App\Service\CurrencyConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\File;

final class HomeController extends AbstractController
{
    public function index(CurrencyConverter $currencyConverter): Response
    {
        $form = $this->createFormBuilder()
            ->add('products', FileType::class, [
                'label' => 'Product Form',
                'mapped' => false,
                'constraints' => new File(
                    maxSize: '8m',
                    mimeTypes: ['application/csv'],
                    mimeTypesMessage: 'Please upload a valid CSV document',
                ),
                'attr' => ['class' => 'form-control-file'],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->getForm();

        /** @var ProductRepository $em */
        $em = $this->getDoctrine()->getRepository(Product::class);

        return $this->renderForm('index.html.twig', [
            'form' => $form,
            'products' => $em->findAll(),
            'avg_cost' => $em->avgCost(),
            'avg_price' => $em->avgPrice(),
            'total_qty' => $em->totalQty(),
            'average_profit_margin' => $em->avgProfitMargin(),
            'total_profit' => $em->totalProfit(),
            'cad_ratio' => $currencyConverter->getRatio('USD', 'CAD'),
        ]);
    }

    public function store(Request $request)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('form')['products'];

        $em = $this->getDoctrine()->getManager();
        $em->getRepository(Product::class)->truncate();

        foreach ((new CsvParser($file))->parse('sku', 'cost', 'price', 'qty') as ['sku' => $sku, 'cost' => $cost, 'price' => $price, 'qty' => $qty]) {
            $em->persist(
                (new Product())
                    ->setSku($sku)
                    ->setCost((float)$cost)
                    ->setPrice((float)$price)
                    ->setQty((int)$qty)
            );
        }

        $em->flush();

        return $this->redirectToRoute('index');
    }
}